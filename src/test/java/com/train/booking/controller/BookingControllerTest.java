package com.train.booking.controller;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.train.booking.dto.BookingReqDto;
import com.train.booking.dto.UserTktBookingDetailsResponseDto;
import com.train.booking.exception.TrainNotFoundException;
import com.train.booking.exception.UserNotFoundException;
import com.train.booking.service.BookingServiceImpl;

@SpringBootTest
class BookingControllerTest {
	
	@InjectMocks
	private BookingController bookingController;
	
	@Mock
	private BookingServiceImpl bookingServiceImpl;
	
	@Before(value = "")
	public void doSetups() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSaveTktBooking() throws UserNotFoundException, TrainNotFoundException {
		@Valid
		BookingReqDto bookingReqDto = new BookingReqDto();
		bookingReqDto.setNoOfTkts("3");
		bookingReqDto.setTicketFare("300");
		bookingReqDto.setTrainNumber("12164");
		bookingReqDto.setUserId("1");
		doReturn(bookingReqDto).when(bookingServiceImpl).saveBooking(bookingReqDto);
		bookingController.saveTktBooking(bookingReqDto );
	}
	
	@Test
	public void testSaveTktBookingElse() throws UserNotFoundException, TrainNotFoundException {
		@Valid
		BookingReqDto bookingReqDto = new BookingReqDto();
		bookingReqDto.setNoOfTkts("6");
		bookingReqDto.setTicketFare("300");
		bookingReqDto.setTrainNumber("12164");
		bookingReqDto.setUserId("1");
		doReturn(bookingReqDto).when(bookingServiceImpl).saveBooking(bookingReqDto);
		bookingController.saveTktBooking(bookingReqDto );
	}
	
	@Test
	public void testgetUserBookingDetails() {
		List<UserTktBookingDetailsResponseDto> list = new ArrayList<>();
		UserTktBookingDetailsResponseDto userBooking = new UserTktBookingDetailsResponseDto();
		userBooking.setTrainNumber("12164");
		list.add(userBooking);
		doReturn(list).when(bookingServiceImpl).userBookingDetails(1);
		bookingController.getUserBookingDetails(1);
	}

}
