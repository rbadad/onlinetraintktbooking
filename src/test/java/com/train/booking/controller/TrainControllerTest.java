package com.train.booking.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import com.train.booking.dto.RunningTrainResponseDto;
import com.train.booking.dto.SearchResponseDto;
import com.train.booking.dto.TrainResponseDto;
import com.train.booking.exception.CanNotSearchForPastDaysException;
import com.train.booking.exception.TrainNotFoundException;
import com.train.booking.exception.TrainsNotAvailableException;
import com.train.booking.exception.TrainsNotAvailableGivenDateException;
import com.train.booking.service.TrainServiceImpl;

class TrainControllerTest {
	
	@InjectMocks
	TrainController trainController;
	@Mock
	TrainServiceImpl trainServiceImpl;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSearch() throws TrainsNotAvailableException, TrainsNotAvailableGivenDateException, CanNotSearchForPastDaysException {
		String source ="chennai";
		String destination = "khammam";
		LocalDate journeyDate = LocalDate.of(2020, 07, 23);
		
		List<SearchResponseDto> responseList = new ArrayList<>();
		SearchResponseDto responseDto1 = new SearchResponseDto();
		responseDto1.setTrainName("Andaman express");
		responseDto1.setTrainNumber(1234);
		
		SearchResponseDto responseDto2 = new SearchResponseDto();
		responseDto2.setTrainName("pushpul express");
		responseDto2.setTrainNumber(3456);
		responseList.add(responseDto1);
		responseList.add(responseDto2);
		
		doReturn(responseList).when(trainServiceImpl).search(source, destination, journeyDate);
		ResponseEntity<List<SearchResponseDto>> response = trainController.search(source, destination, journeyDate);
		assertEquals("Andaman express", response.getBody().get(0).getTrainName());
		assertEquals(3456, response.getBody().get(1).getTrainNumber());
		
	}
	
	@Test
	public void testGetById() throws TrainNotFoundException {
		Integer trainId =1;
		TrainResponseDto responseDto = new TrainResponseDto();
		responseDto.setArrivalTime(LocalDateTime.of(LocalDate.of(2020, 07, 24), LocalTime.of(6, 30)));
		responseDto.setDepartureTime(LocalDateTime.of(LocalDate.of(2020, 07, 23), LocalTime.of(8, 30)));
		responseDto.setDestination("chennai");
		responseDto.setDuration("10");
		responseDto.setFare(300);
		responseDto.setSource("khammam");
		responseDto.setTrainId(1);
		responseDto.setTrainName("Andaman express");
		responseDto.setTrainNumber(1234);
		
		List<RunningTrainResponseDto> runningTrainDetails = new ArrayList<>();
		RunningTrainResponseDto rtResponse = new RunningTrainResponseDto();
		rtResponse.setJourneyDate(LocalDate.of(2020, 07, 23));
		rtResponse.setNoOfAvailableSeats(70);
		rtResponse.setReservationClass("AC");
		runningTrainDetails.add(rtResponse);
		
		responseDto.setRunningTrainDetails(runningTrainDetails);
		
		doReturn(responseDto).when(trainServiceImpl).getById(trainId);
		ResponseEntity<TrainResponseDto> response =trainController.getById(trainId);
		assertEquals("AC", response.getBody().getRunningTrainDetails().get(0).getReservationClass());
		
		
	}
	
	

}
