package com.train.booking.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.train.booking.dto.PaymentRequestDto;
import com.train.booking.dto.PaymentResponseDto;
import com.train.booking.entity.Payment;
import com.train.booking.entity.User;
import com.train.booking.repository.PaymentRepository;
import com.train.booking.repository.UserRepository;

/**
 * @author prabirkumar.jena
 *
 * PhonePayService Test class
 */
public class PhonePayServiceTest {

	@InjectMocks
	PhonePayService paymentService;
	@Mock
	PaymentRepository paymentRepository;
	@Mock
	UserRepository userRepository;
	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetPhonePayServiceFailure() {
		User user = new User();
		user.setUserId(1);
		PaymentRequestDto paymentRequestDto = new PaymentRequestDto();
		paymentRequestDto.setUserId(1);
		paymentRequestDto.setTotalAmount(1000);
		paymentRequestDto.setModeOfPayment("GPay");
		when(userRepository.findById(paymentRequestDto.getUserId())).thenReturn(Optional.of(user));
		Payment payment = new Payment();
		payment.setModeOfPayment(paymentRequestDto.getModeOfPayment());
		payment.setPaymantDate(LocalDate.now());
		payment.setTotalAmount(paymentRequestDto.getTotalAmount());
		payment.setUser(user);
		when(paymentRepository.save(payment)).thenReturn(payment);
		PaymentResponseDto paymentResponseDto = paymentService.updatePaymentService(paymentRequestDto);
		assertEquals("2001", paymentResponseDto.getStatusCode());
		assertEquals("payment failed", paymentResponseDto.getStatusMessage());
	}

	@Test
	public void testGetPhonePayServiceSuccess() {
		User user = new User();
		user.setUserId(1);
		PaymentRequestDto paymentRequestDto = new PaymentRequestDto();
		paymentRequestDto.setUserId(1);
		paymentRequestDto.setTotalAmount(1000);
		paymentRequestDto.setModeOfPayment("PhonePay");
		when(userRepository.findById(paymentRequestDto.getUserId())).thenReturn(Optional.of(user));
		Payment payment = new Payment();
		payment.setModeOfPayment(paymentRequestDto.getModeOfPayment());
		payment.setPaymantDate(LocalDate.now());
		payment.setTotalAmount(paymentRequestDto.getTotalAmount());
		payment.setUser(user);
		when(paymentRepository.save(ArgumentMatchers.<Payment>any())).thenReturn(payment);
		PaymentResponseDto paymentResponseDto = paymentService.updatePaymentService(paymentRequestDto);
		assertEquals("2000", paymentResponseDto.getStatusCode());
		assertEquals("payment done successfully", paymentResponseDto.getStatusMessage());
	}

}
