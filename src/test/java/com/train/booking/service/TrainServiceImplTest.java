package com.train.booking.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.train.booking.dto.RunningTrainResponseDto;
import com.train.booking.dto.TrainResponseDto;
import com.train.booking.entity.RunningTrain;
import com.train.booking.entity.Train;
import com.train.booking.exception.CanNotSearchForPastDaysException;
import com.train.booking.exception.TrainNotFoundException;
import com.train.booking.exception.TrainsNotAvailableException;
import com.train.booking.exception.TrainsNotAvailableGivenDateException;
import com.train.booking.repository.RunningTrainRepository;
import com.train.booking.repository.TrainRepository;

class TrainServiceImplTest {

	@InjectMocks
	TrainServiceImpl trainServiceImpl;
	
	@Mock
	TrainRepository trainRepository;
	@Mock
	RunningTrainRepository runningTrainRepository;
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSearch1()  {
		String source ="chennai";
		String destination = "khammam";
		LocalDate journeyDate = LocalDate.of(2020, 07, 23);
		
		List<Train> trainList = new ArrayList<>();
		try {
			doReturn(trainList).when(trainRepository).findBySourceAndDestination(source, destination);
			trainServiceImpl.search(source, destination, journeyDate);
		} catch (TrainsNotAvailableException | TrainsNotAvailableGivenDateException | CanNotSearchForPastDaysException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testSearch2()  {
		String source ="chennai";
		String destination = "khammam";
		LocalDate journeyDate = LocalDate.of(2020, 07, 23);
		

		List<Train> trainList = new ArrayList<>();
		Train train = new Train();
		train.setArrivalTime(LocalTime.of(6, 30));
		train.setDepartureTime(LocalTime.of(8, 30));
		train.setDestination("chennai");
		train.setDuration("10");
		train.setFare(300);
		train.setSource("khammam");
		train.setTrainId(1);
		train.setTrainName("Andaman express");
		train.setTrainNumber(1234);
		trainList.add(train);
		
		List<RunningTrain> runningTrainList = new ArrayList<>();

		try {
			//when(trainRepository.findBySourceAndDestination(source, destination)).thenReturn(trainList);
			doReturn(trainList).when(trainRepository).findBySourceAndDestination(source, destination);
			doReturn(runningTrainList).when(runningTrainRepository).findByJourneyDate(journeyDate);
			trainServiceImpl.search(source, destination, journeyDate);
		} catch (TrainsNotAvailableException | TrainsNotAvailableGivenDateException | CanNotSearchForPastDaysException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testSearch3()  {
		String source ="chennai";
		String destination = "khammam";
		LocalDate journeyDate = LocalDate.of(2020, 07, 22);
		
		List<Train> trainList = new ArrayList<>();
		Train train = new Train();
		train.setArrivalTime(LocalTime.of(6, 30));
		train.setDepartureTime(LocalTime.of(8, 30));
		train.setDestination("chennai");
		train.setDuration("10");
		train.setFare(300);
		train.setSource("khammam");
		train.setTrainId(1);
		train.setTrainName("Andaman express");
		train.setTrainNumber(1234);
		trainList.add(train);
		
		try {
			doReturn(trainList).when(trainRepository).findBySourceAndDestination(source, destination);
			trainServiceImpl.search(source, destination, journeyDate);
		} catch (TrainsNotAvailableException | TrainsNotAvailableGivenDateException | CanNotSearchForPastDaysException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	@Test
	public void testSearch4() {
		String source ="chennai";
		String destination = "khammam";
		LocalDate journeyDate = LocalDate.of(2020, 07, 23);
		
		List<Train> trainList = new ArrayList<>();
		Train train = new Train();
		train.setArrivalTime(LocalTime.of(6, 30));
		train.setDepartureTime(LocalTime.of(8, 30));
		train.setDestination("chennai");
		train.setDuration("10");
		train.setFare(300);
		train.setSource("khammam");
		train.setTrainId(1);
		train.setTrainName("Andaman express");
		train.setTrainNumber(1234);
		trainList.add(train);
		
		List<RunningTrain> runningTrainList = new ArrayList<>();
		RunningTrain runningTrain = new RunningTrain();
		runningTrain.setJourneyDate(LocalDate.of(2020, 07, 23));
		runningTrain.setNoOfAvailableSeats(70);
		runningTrain.setReservationClass("AC");
		runningTrainList.add(runningTrain);
		
		
		when(trainRepository.findBySourceAndDestination(source, destination)).thenReturn(trainList);
		
		when(runningTrainRepository.findByJourneyDate(journeyDate)).thenReturn(runningTrainList);
		
		TrainResponseDto responseDto = new TrainResponseDto();
		responseDto.setArrivalTime(LocalDateTime.of(LocalDate.of(2020, 07, 24), LocalTime.of(6, 30)));
		responseDto.setDepartureTime(LocalDateTime.of(LocalDate.of(2020, 07, 23), LocalTime.of(8, 30)));
		responseDto.setDestination("chennai");
		responseDto.setDuration("10");
		responseDto.setFare(300);
		responseDto.setSource("khammam");
		responseDto.setTrainId(1);
		responseDto.setTrainName("Andaman express");
		responseDto.setTrainNumber(1234);
		
		List<RunningTrainResponseDto> runningTrainDetails = new ArrayList<>();
		RunningTrainResponseDto rtResponse = new RunningTrainResponseDto();
		rtResponse.setJourneyDate(LocalDate.of(2020, 07, 23));
		rtResponse.setNoOfAvailableSeats(70);
		rtResponse.setReservationClass("AC");
		runningTrainDetails.add(rtResponse);
		
		responseDto.setRunningTrainDetails(runningTrainDetails);
		
		assertEquals("Andaman express", responseDto.getTrainName());
		assertEquals(1234, responseDto.getTrainNumber());
		
	}
	
	
	
	@Test
	public void testGetById1() {
		Integer trainId =1;
		Train train = new Train();
		Optional<Train> trainObj = Optional.of(train);
		/*train.setArrivalTime(LocalTime.of(6, 30));
		train.setDepartureTime(LocalTime.of(8, 30));
		train.setDestination("chennai");
		train.setDuration("10");
		train.setFare(300);
		train.setSource("khammam");
		train.setTrainId(1);
		train.setTrainName("Andaman express");
		train.setTrainNumber(1234);*/
		try {
			doReturn(trainObj).when(trainRepository).findById(trainId);
			trainServiceImpl.getById(trainId);
		} catch (TrainNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testGetById2() {
		Integer trainId =1;
		Train train = new Train();
		train.setArrivalTime(LocalTime.of(6, 30));
		train.setDepartureTime(LocalTime.of(8, 30));
		train.setDestination("chennai");
		train.setDuration("10");
		train.setFare(300);
		train.setSource("khammam");
		train.setTrainId(1);
		train.setTrainName("Andaman express");
		train.setTrainNumber(1234);
		when(trainRepository.findById(trainId)).thenReturn(Optional.of(train));
		

		List<RunningTrain> runningTrainList = new ArrayList<>();
		RunningTrain runningTrain = new RunningTrain();
		runningTrain.setJourneyDate(LocalDate.of(2020, 07, 23));
		runningTrain.setNoOfAvailableSeats(70);
		runningTrain.setReservationClass("AC");
		runningTrainList.add(runningTrain);
		when(runningTrainRepository.findByTrain(train)).thenReturn(runningTrainList);
		
		TrainResponseDto responseDto = new TrainResponseDto();
		responseDto.setArrivalTime(LocalDateTime.of(LocalDate.of(2020, 07, 24), LocalTime.of(6, 30)));
		responseDto.setDepartureTime(LocalDateTime.of(LocalDate.of(2020, 07, 23), LocalTime.of(8, 30)));
		responseDto.setDestination("chennai");
		responseDto.setDuration("10");
		responseDto.setFare(300);
		responseDto.setSource("khammam");
		responseDto.setTrainId(1);
		responseDto.setTrainName("Andaman express");
		responseDto.setTrainNumber(1234);
		
		List<RunningTrainResponseDto> runningTrainDetails = new ArrayList<>();
		RunningTrainResponseDto rtResponse = new RunningTrainResponseDto();
		rtResponse.setJourneyDate(LocalDate.of(2020, 07, 23));
		rtResponse.setNoOfAvailableSeats(70);
		rtResponse.setReservationClass("AC");
		runningTrainDetails.add(rtResponse);
		responseDto.setRunningTrainDetails(runningTrainDetails);
		assertEquals("AC", responseDto.getRunningTrainDetails().get(0).getReservationClass());
		
	}
	
	
}
