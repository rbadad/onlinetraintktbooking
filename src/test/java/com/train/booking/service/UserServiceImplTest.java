package com.train.booking.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.train.booking.constants.AppConstants;
import com.train.booking.dto.UserLoginReqDto;
import com.train.booking.dto.UserLoginResponseDto;
import com.train.booking.entity.User;
import com.train.booking.repository.UserRepository;

/**
 * @author prabirkumar.jena
 *
 * UserServiceImpl Test class
 */
public class UserServiceImplTest {

	@InjectMocks
	UserServiceImpl userService;
	@Mock
	UserRepository userRepository;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testLoginValidationSuccess() {
		UserLoginReqDto userLoginReqDto = new UserLoginReqDto();
		userLoginReqDto.setEmail("prabir@gmail.com");
		userLoginReqDto.setPassword("123456");

		User user = new User();
		user.setEmail("prabir@gmail.com");
		user.setPassword("123456");
		when(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword())).thenReturn(user);

		UserLoginResponseDto customerLoginResponseDto = userService.userLogin(userLoginReqDto);
		assertEquals(AppConstants.LOGIN_SUCCESS, customerLoginResponseDto.getStatusMsg());
		assertEquals(AppConstants.LOGIN_SUCCESS_STATUS_CODE, customerLoginResponseDto.getStatusCode());
	}

	@Test
	public void testLoginValidationFailure() {
		UserLoginReqDto userLoginReqDto = new UserLoginReqDto();
		userLoginReqDto.setEmail("prabir@gmail.com");
		userLoginReqDto.setPassword("123456");

		User user = new User();
		user.setEmail("prabir@gmail.com");
		user.setPassword("1234561");
		when(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword())).thenReturn(user);

		UserLoginResponseDto userLoginResponseDto = userService.userLogin(userLoginReqDto);

		assertEquals(AppConstants.LOGIN_FAILURE, userLoginResponseDto.getStatusMsg());
		assertEquals(AppConstants.LOGIN_FAILURE_STATUS_CODE, userLoginResponseDto.getStatusCode());
	}

}
