package com.train.booking.service;

import static org.mockito.Mockito.doReturn;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.train.booking.dto.BookingReqDto;
import com.train.booking.entity.BookingDetails;
import com.train.booking.exception.TrainNotFoundException;
import com.train.booking.exception.UserNotFoundException;
import com.train.booking.repository.BookingRepository;

@SpringBootTest
class BookingServiceImplTest {
	
	@InjectMocks
	private BookingServiceImpl bookingServiceImpl;

	@Mock
	private BookingRepository bookingRepository;
	
	@Before(value = "")
	public void doSetups() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testSaveBooking() {
		BookingReqDto bookingReqDto = new BookingReqDto();
		bookingReqDto.setNoOfTkts("3");
		bookingReqDto.setTicketFare("300");
		bookingReqDto.setTrainNumber("12164");
		bookingReqDto.setUserId("1");
		try {
			doReturn(bookingReqDto).when(bookingRepository).save(new BookingDetails());
			bookingServiceImpl.saveBooking(bookingReqDto);
		} catch (UserNotFoundException | TrainNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testuserBookingDetails() {
		bookingServiceImpl.userBookingDetails(1);
	}

}
