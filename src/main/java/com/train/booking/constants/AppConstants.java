package com.train.booking.constants;

public class AppConstants {

	public static final String LOGIN_SUCCESS = "Login successful";
	public static final String LOGIN_SUCCESS_STATUS_CODE = "5000";
	public static final String LOGIN_FAILURE = "Login failed, please enter valid credentials";
	public static final String LOGIN_FAILURE_STATUS_CODE = "5001";
	public static final String WRONG_EMAIL = "Please provide valid email";
	public static final String WRONG_PASSWORD = "Please provide valid password";
	public static final String WRONG_EMAIL_STATUSCODE = "100";
	public static final String WRONG_PASSWORD_STATUSCODE = "101";
	public static final String USER_NOT_FOUND_CODE = "2511";
	public static final String USER_NOT_FOUND = "User not found";
	public static final String PAYMENT_STATUS_CODE = "2000";
	public static final String NO_PAYMENT_STATUS_CODE = "2001";
	public static final String PAYMENT_FAILED_STATUS_MSG = "payment failed";
	public static final String PAYMENT_SUCCESS_STATUS_MSG = "payment done successfully";
	public static final String BOOKING_CODE = "1001";
	public static final String BOOKING_MSG = "TRAIN TICKET BOOKED SUCCESSFULLY";
	public static final String BOOKING_FAILURE_CODE = "1002";
	public static final String BOOKING_FAILURE_MSG = "TRAIN TICKET NOT BOOKED, SOMETHING WENT WRONG";
	public static final String BOOKED = "BOOKED";
	public static final String TRAIN_NOT_FOUND = "1002";
	public static final String TRAIN_NOT_FOUND_MSG = "PLEASE ENTER THE VALID TRAIN NUMBER";
	public static final Integer FIVE = 5;
	public static final String MAX_BOOKING_VALIDATION_CODE = "2002";
	public static final String MAX_BOOKING_VALIDATION_MSG = "YOU CAN'T BOOK MORE THAN FIVE SEATS RESERVATION AT SAME TIME";
	public static final String TRAIN_NOT_FOUND1 = "Train is not available";
	public static final int TRAIN_NOT_FOUND_STATUS_CODE = 4000;
	public static final String TRAINS_NOT_AVAILABLE = "Trains are not available on this route";
	public static final int TRAINS_NOT_AVAILABLE_STATUS_CODE = 4001;
	public static final String TRAINS_NOT_AVAILABLE_GIVEN_DATE = "Trains are not available for given date";
	public static final int TRAINS_NOT_AVAILABLE_GIVEN_DATE_STATUS_CODE = 4002;
	
	
	public static final String CAN_NOT_SEARCH_FOR_PAST_DATE = "Can not search for past days, please select proper date!";
	public static final int CAN_NOT_SEARCH_FOR_PAST_DATE_STATUS_CODE = 4003;

	private AppConstants() {
		super();
	}
	
	public static final String TICKET_COUNT_CODE = "2600";
	public static final String TICKET_COUNT_MSG = "Available tickets are lesser than booking tickets";
}
