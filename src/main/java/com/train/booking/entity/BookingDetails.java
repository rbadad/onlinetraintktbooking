package com.train.booking.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * @author Raja
 *
 */
@Entity
@Table(name = "BOOKKING_DETAILS")
public class BookingDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer bookingId;
	@OneToOne
	@JoinColumn(name = "userId")
	private User user;
	@OneToOne
	@JoinColumn(name = "trainId")
	private Train trainId;
	private Integer noOfTicket;
	private double totalFare;
	private String status;
	private String pnr;
	public BookingDetails() {
		super();
	}
	public BookingDetails(Integer bookingId, User user, Train trainId, Integer noOfTicket, double totalFare,
			String status, String pnr) {
		super();
		this.bookingId = bookingId;
		this.user = user;
		this.trainId = trainId;
		this.noOfTicket = noOfTicket;
		this.totalFare = totalFare;
		this.status = status;
		this.pnr = pnr;
	}
	public Integer getBookingId() {
		return bookingId;
	}
	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Train getTrainId() {
		return trainId;
	}
	public void setTrainId(Train trainId) {
		this.trainId = trainId;
	}
	public Integer getNoOfTicket() {
		return noOfTicket;
	}
	public void setNoOfTicket(Integer noOfTicket) {
		this.noOfTicket = noOfTicket;
	}
	public double getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	@Override
	public String toString() {
		return "BookingDetails [bookingId=" + bookingId + ", user=" + user + ", trainId=" + trainId + ", noOfTicket="
				+ noOfTicket + ", totalFare=" + totalFare + ", status=" + status + ", pnr=" + pnr + "]";
	}	
}

