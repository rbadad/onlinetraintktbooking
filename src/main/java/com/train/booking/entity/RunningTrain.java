package com.train.booking.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "RUNNING_TRAIN")
@DynamicUpdate(value = true)
public class RunningTrain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer runningTrainId;
	@OneToOne
	@JoinTable(name = "trainId")
	private Train train;
	private LocalDate journeyDate;
	private Integer noOfAvailableSeats;
	private String reservationClass;

	public RunningTrain() {
		super();
	}

	public RunningTrain(Integer runningTrainId, Train train, LocalDate journeyDate, Integer noOfAvailableSeats,
			String reservationClass) {
		super();
		this.runningTrainId = runningTrainId;
		this.train = train;
		this.journeyDate = journeyDate;
		this.noOfAvailableSeats = noOfAvailableSeats;
		this.reservationClass = reservationClass;
	}

	public Integer getRunningTrainId() {
		return runningTrainId;
	}

	public void setRunningTrainId(Integer runningTrainId) {
		this.runningTrainId = runningTrainId;
	}

	public Train getTrain() {
		return train;
	}

	public void setTrain(Train train) {
		this.train = train;
	}

	public LocalDate getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(LocalDate journeyDate) {
		this.journeyDate = journeyDate;
	}

	public Integer getNoOfAvailableSeats() {
		return noOfAvailableSeats;
	}

	public void setNoOfAvailableSeats(Integer noOfAvailableSeats) {
		this.noOfAvailableSeats = noOfAvailableSeats;
	}

	public String getReservationClass() {
		return reservationClass;
	}

	public void setReservationClass(String reservationClass) {
		this.reservationClass = reservationClass;
	}

	@Override
	public String toString() {
		return "RunningTrain [runningTrainId=" + runningTrainId + ", train=" + train + ", journeyDate=" + journeyDate
				+ ", noOfAvailableSeats=" + noOfAvailableSeats + ", reservationClass=" + reservationClass + "]";
	}
}
