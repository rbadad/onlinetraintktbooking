package com.train.booking.dto;

/**
 * @author prabirkumar.jena
 *
 * PaymentResponseDto class
 */
public class PaymentResponseDto {

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	private String statusCode;
	private String statusMessage;

}
