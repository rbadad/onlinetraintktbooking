package com.train.booking.dto;

import java.time.LocalDate;

/**
 * @author janbee
 *
 */
public class RunningTrainResponseDto {

	private LocalDate journeyDate;
	private Integer noOfAvailableSeats;
	private String reservationClass;
	public LocalDate getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(LocalDate journeyDate) {
		this.journeyDate = journeyDate;
	}
	public Integer getNoOfAvailableSeats() {
		return noOfAvailableSeats;
	}
	public void setNoOfAvailableSeats(Integer noOfAvailableSeats) {
		this.noOfAvailableSeats = noOfAvailableSeats;
	}
	public String getReservationClass() {
		return reservationClass;
	}
	public void setReservationClass(String reservationClass) {
		this.reservationClass = reservationClass;
	}
	
}
