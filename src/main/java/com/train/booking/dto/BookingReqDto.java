package com.train.booking.dto;

public class BookingReqDto {

	private String userId;
	private String trainNumber;
	private String noOfTkts;
	private String ticketFare;
	
	public BookingReqDto() {
		super();
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}
	public String getNoOfTkts() {
		return noOfTkts;
	}
	public void setNoOfTkts(String noOfTkts) {
		this.noOfTkts = noOfTkts;
	}
	public String getTicketFare() {
		return ticketFare;
	}
	public void setTicketFare(String ticketFare) {
		this.ticketFare = ticketFare;
	}
}
