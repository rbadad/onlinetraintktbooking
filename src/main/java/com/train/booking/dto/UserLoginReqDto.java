package com.train.booking.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;



/**
 * @author prabirkumar.jena
 *
 * UserLoginReqDto class
 */
public class UserLoginReqDto {

	@Email
	@NotEmpty(message = "Please provide the valid email")
	private String email;
	@NotEmpty(message = "password is mandatory")
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
