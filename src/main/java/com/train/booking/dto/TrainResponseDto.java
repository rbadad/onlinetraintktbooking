package com.train.booking.dto;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author janbee
 * 
 * TrainResponseDto dto class
 *
 */
public class TrainResponseDto {

	private Integer trainId;
	private String trainName;
	private LocalDateTime departureTime;
	private LocalDateTime arrivalTime;
	private String source;
	private String destination;
	private String duration;
	private double fare;
	private Integer trainNumber;
	private List<RunningTrainResponseDto> runningTrainDetails;
	
	public Integer getTrainId() {
		return trainId;
	}
	public void setTrainId(Integer trainId) {
		this.trainId = trainId;
	}
	public String getTrainName() {
		return trainName;
	}
	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public double getFare() {
		return fare;
	}
	public void setFare(double fare) {
		this.fare = fare;
	}
	public Integer getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(Integer trainNumber) {
		this.trainNumber = trainNumber;
	}
	public LocalDateTime getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(LocalDateTime departureTime) {
		this.departureTime = departureTime;
	}
	public LocalDateTime getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(LocalDateTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public List<RunningTrainResponseDto> getRunningTrainDetails() {
		return runningTrainDetails;
	}
	public void setRunningTrainDetails(List<RunningTrainResponseDto> runningTrainDetails) {
		this.runningTrainDetails = runningTrainDetails;
	}
	public String getDuration() {
		return duration;
	}
	
	
}

