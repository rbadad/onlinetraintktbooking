package com.train.booking.dto;

public class UserTktBookingDetailsResponseDto {

	private String trainName;
	private String trainNumber;
	private String noOfTkts;
	private String totalFare;
	private String status;
	private String pnr;
	
	public UserTktBookingDetailsResponseDto() {
		super();
	}
	public String getTrainName() {
		return trainName;
	}
	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}
	public String getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}
	public String getNoOfTkts() {
		return noOfTkts;
	}
	public void setNoOfTkts(String noOfTkts) {
		this.noOfTkts = noOfTkts;
	}
	public String getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}

