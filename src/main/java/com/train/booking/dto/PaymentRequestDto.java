package com.train.booking.dto;

import javax.validation.constraints.NotEmpty;

/**
 * @author prabirkumar.jena
 * 
 * PaymentRequestDto class
 */
public class PaymentRequestDto {
	private Integer userId;
	private double totalAmount;
	@NotEmpty(message = "modeOfPayment is Mandatory")
	private String modeOfPayment;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
}
