package com.train.booking.dto;

/**
 * @author prabirkumar.jena
 *
 * UserLoginResponseDto class
 */
public class UserLoginResponseDto {

	private String statusCode;
	private String statusMsg;

	public UserLoginResponseDto() {
		super();
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

}
