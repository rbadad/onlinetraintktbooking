package com.train.booking.dto;

/**
 * @author janbee
 * 
 * SearchResponseDto dto class
 *
 */
public class SearchResponseDto {

	private String trainName;
	private Integer trainNumber;
	public String getTrainName() {
		return trainName;
	}
	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}
	public Integer getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(Integer trainNumber) {
		this.trainNumber = trainNumber;
	}
	
}
