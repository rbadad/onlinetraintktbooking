package com.train.booking.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.train.booking.dto.RunningTrainResponseDto;
import com.train.booking.dto.SearchResponseDto;
import com.train.booking.dto.TrainResponseDto;
import com.train.booking.entity.RunningTrain;
import com.train.booking.entity.Train;
import com.train.booking.exception.CanNotSearchForPastDaysException;
import com.train.booking.exception.TrainNotFoundException;
import com.train.booking.exception.TrainsNotAvailableException;
import com.train.booking.exception.TrainsNotAvailableGivenDateException;
import com.train.booking.repository.RunningTrainRepository;
import com.train.booking.repository.TrainRepository;

/**
 * 
 * @author janbee
 *
 */
@Service
public class TrainServiceImpl implements TrainService{

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainServiceImpl.class);
	@Autowired
	TrainRepository trainRepository;
	@Autowired
	RunningTrainRepository runningTrainRepository;

	@Override
	public List<SearchResponseDto> search(String source, String destination, LocalDate journeyDate) throws TrainsNotAvailableException, TrainsNotAvailableGivenDateException, CanNotSearchForPastDaysException {
		LOGGER.info("TrainServiceImpl :: search :: start");
		List<Train> trainList = trainRepository.findBySourceAndDestination(source, destination);
		if(trainList.isEmpty()) {
			throw new TrainsNotAvailableException();
		}
		int diff = journeyDate.compareTo(LocalDate.now());
		if(diff<0) {
			throw new CanNotSearchForPastDaysException();
		}
		List<RunningTrain> runningTrainList = runningTrainRepository.findByJourneyDate(journeyDate);
		if(runningTrainList.isEmpty()) {
			throw new TrainsNotAvailableGivenDateException();
		}
		List<SearchResponseDto> responseList = new ArrayList<>();
		for(RunningTrain rt : runningTrainList) {
			int trainId = rt.getTrain().getTrainId();
			for(Train t : trainList) {
				int trainId1 = t.getTrainId();
				if(trainId==trainId1) {
					SearchResponseDto responseDto = new SearchResponseDto();
					BeanUtils.copyProperties(t, responseDto);
					responseList.add(responseDto);
				}
			}
		}
		LOGGER.info("TrainServiceImpl :: search :: end");
		return responseList;
	}

	@Override
	public TrainResponseDto getById(Integer trainId) throws TrainNotFoundException {
		LOGGER.info("TrainServiceImpl :: getById :: start");
		TrainResponseDto responseDto;
		Train train = trainRepository.findByTrainNumber(trainId);
		/*
		 * if(!train.isPresent()) { throw new TrainNotFoundException(); }
		 */
		Train trainObj = train;
		responseDto = new TrainResponseDto(); 
		List<RunningTrain> runningTrainList = runningTrainRepository.findByTrain(trainObj);
		BeanUtils.copyProperties(trainObj, responseDto);
		List<RunningTrainResponseDto> rtResponseDtoList = new ArrayList<>();
		
		for(RunningTrain rt : runningTrainList) {
			RunningTrainResponseDto rtResponseDto = new RunningTrainResponseDto();
			BeanUtils.copyProperties(rt, rtResponseDto);
			responseDto.setRunningTrainDetails(rtResponseDtoList);
			rtResponseDtoList.add(rtResponseDto);
		}
		responseDto.setRunningTrainDetails(rtResponseDtoList);
		LOGGER.info("TrainServiceImpl :: getById :: end");
		return responseDto;
	}

}
