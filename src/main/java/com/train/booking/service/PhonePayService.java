package com.train.booking.service;

import java.time.LocalDate;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.train.booking.constants.AppConstants;
import com.train.booking.dto.PaymentRequestDto;
import com.train.booking.dto.PaymentResponseDto;
import com.train.booking.entity.Payment;
import com.train.booking.entity.User;
import com.train.booking.repository.PaymentRepository;
import com.train.booking.repository.UserRepository;

/**
 * @author prabirkumar.jena
 *
 * PhonePayService Implementation Class
 */
@Service
@Transactional
public class PhonePayService implements PaymentService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PhonePayService.class);
	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	UserRepository userRepository;

	public PaymentResponseDto updatePaymentService(PaymentRequestDto paymentRequestDto) {
		LOGGER.info("PhonePayService updatePaymentService() method called");
		Optional<User> user = userRepository.findById(paymentRequestDto.getUserId());
		Payment payment = new Payment();
		payment.setModeOfPayment(paymentRequestDto.getModeOfPayment());
		payment.setPaymantDate(LocalDate.now());
		payment.setTotalAmount(paymentRequestDto.getTotalAmount());
		if (user.isPresent()) {
			payment.setUser(user.get());
		}
		Payment payment2 = paymentRepository.save(payment);
		PaymentResponseDto paymentResponseDto = new PaymentResponseDto();
		if (payment2 != null) {
			paymentResponseDto.setStatusCode(AppConstants.PAYMENT_STATUS_CODE);
			paymentResponseDto.setStatusMessage(AppConstants.PAYMENT_SUCCESS_STATUS_MSG);
			LOGGER.info("PhonePayService Payment Successful");
		} else {
			paymentResponseDto.setStatusCode(AppConstants.NO_PAYMENT_STATUS_CODE);
			paymentResponseDto.setStatusMessage(AppConstants.PAYMENT_FAILED_STATUS_MSG);
			LOGGER.info("PhonePayService Payment Failed");
		}
		return paymentResponseDto;
	}

}