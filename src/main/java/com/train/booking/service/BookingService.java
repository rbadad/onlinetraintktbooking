package com.train.booking.service;

import com.train.booking.dto.BookingReqDto;
import com.train.booking.dto.BookingResDto;
import com.train.booking.exception.TrainNotFoundException;
import com.train.booking.exception.UserNotFoundException;


/**
 * @author Raja
 *
 */
public interface BookingService {

	public BookingResDto saveBooking(BookingReqDto bookingReqDto) throws UserNotFoundException, TrainNotFoundException;
}
