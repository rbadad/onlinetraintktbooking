package com.train.booking.service;

import java.time.LocalDate;
import java.util.List;

import com.train.booking.dto.SearchResponseDto;
import com.train.booking.dto.TrainResponseDto;
import com.train.booking.exception.CanNotSearchForPastDaysException;
import com.train.booking.exception.TrainNotFoundException;
import com.train.booking.exception.TrainsNotAvailableException;
import com.train.booking.exception.TrainsNotAvailableGivenDateException;

/**
 * 
 * @author janbee
 *
 */
public interface TrainService {

	/**
	 * 
	 * @param source
	 * @param destination
	 * @param journeyDate
	 * @return List<SearchResponseDto>
	 * @throws TrainsNotAvailableException
	 */
	public List<SearchResponseDto> search(String source, String destination, LocalDate journeyDate) throws TrainsNotAvailableException,
	TrainsNotAvailableGivenDateException,CanNotSearchForPastDaysException;
	
	/**
	 * 
	 * @param trainId
	 * @return TrainResponseDto
	 * @throws TrainNotFoundException
	 */
	public TrainResponseDto getById(Integer trainId) throws TrainNotFoundException;
}
