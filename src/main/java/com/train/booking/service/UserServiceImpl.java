package com.train.booking.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.train.booking.constants.AppConstants;
import com.train.booking.dto.UserLoginReqDto;
import com.train.booking.dto.UserLoginResponseDto;
import com.train.booking.entity.User;
import com.train.booking.repository.UserRepository;

/**
 * @author prabirkumar.jena
 *
 */
@Service
public class UserServiceImpl implements UserService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	private UserRepository repository;

	@Override
	public UserLoginResponseDto userLogin(UserLoginReqDto reqDto) {
		LOGGER.info("User Login method start");
		UserLoginResponseDto response = new UserLoginResponseDto();
		User customer = repository.findByEmailAndPassword(reqDto.getEmail(), reqDto.getPassword());
		if (customer != null) {
			response.setStatusCode(AppConstants.LOGIN_SUCCESS_STATUS_CODE);
			response.setStatusMsg(AppConstants.LOGIN_SUCCESS);
			LOGGER.info("User Login Successful");
		} else {
			response.setStatusCode(AppConstants.LOGIN_FAILURE_STATUS_CODE);
			response.setStatusMsg(AppConstants.LOGIN_FAILURE);
			LOGGER.info("User Login Failed");
		}
		return response;
	}

}
