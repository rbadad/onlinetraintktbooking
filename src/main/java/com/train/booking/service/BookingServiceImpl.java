package com.train.booking.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.LockModeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.train.booking.constants.AppConstants;
import com.train.booking.dto.BookingReqDto;
import com.train.booking.dto.BookingResDto;
import com.train.booking.dto.UserTktBookingDetailsResponseDto;
import com.train.booking.entity.BookingDetails;
import com.train.booking.entity.Payment;
import com.train.booking.entity.RunningTrain;
import com.train.booking.entity.Train;
import com.train.booking.entity.User;
import com.train.booking.exception.TrainNotFoundException;
import com.train.booking.exception.UserNotFoundException;
import com.train.booking.repository.BookingRepository;
import com.train.booking.repository.PaymentRepository;
import com.train.booking.repository.RunningTrainRepository;
import com.train.booking.repository.TrainRepository;
import com.train.booking.repository.UserRepository;

@Service
public class BookingServiceImpl implements BookingService{

	private static final Logger LOGGER = LoggerFactory.getLogger(BookingServiceImpl.class);
	@Autowired
	private BookingRepository bookingRepository;

	@Autowired
	private TrainRepository trainrRepo;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PaymentRepository paymentRepository;
	
	@Autowired
	private RunningTrainRepository runningTrainRepo;

	// here we are booking tkts and validating userId and trainNumber before booking tkts
	@Transactional(isolation = Isolation.SERIALIZABLE)
	@Lock(LockModeType.OPTIMISTIC)
	@Override
	public BookingResDto saveBooking(BookingReqDto bookingReqDto) throws UserNotFoundException, TrainNotFoundException{
		LOGGER.info("*************Inside saveBooking  service layer***************START");
		BookingResDto reponse = new BookingResDto();
		BookingDetails booking = new BookingDetails();
		try {
			Train train = trainrRepo.findByTrainNumber(Integer.parseInt(bookingReqDto.getTrainNumber()));
			if(null!=train) {
				booking.setTrainId(train);
			}else {
				reponse.setStatusCode(AppConstants.TRAIN_NOT_FOUND);
				reponse.setStatusMsg(AppConstants.TRAIN_NOT_FOUND_MSG);
				return reponse;
			}
			Optional<User> user = userRepository.findById(Integer.parseInt(bookingReqDto.getUserId()));
			if(user.isPresent()) {
				booking.setUser(user.get());
			}else {
				reponse.setStatusCode(AppConstants.USER_NOT_FOUND_CODE);
				reponse.setStatusMsg(AppConstants.USER_NOT_FOUND);
				return reponse;
			}
			List<RunningTrain> runningTrainList = runningTrainRepo.findByTrain(train);
			LOGGER.info("runningTrain"+ runningTrainList);
			for(RunningTrain rt : runningTrainList) {
				int seats = rt.getNoOfAvailableSeats();
				if(Integer.parseInt(bookingReqDto.getNoOfTkts())<=seats){
					int afterBookingAvailableSeats = (seats-(Integer.parseInt(bookingReqDto.getNoOfTkts())));
					rt.setNoOfAvailableSeats(afterBookingAvailableSeats);
					booking.setNoOfTicket(Integer.parseInt(bookingReqDto.getNoOfTkts()));
					runningTrainRepo.save(rt);
				}else {
					reponse.setStatusCode(AppConstants.TICKET_COUNT_CODE);
					reponse.setStatusMsg(AppConstants.TICKET_COUNT_MSG);
					return reponse;
				}
			}
			
			booking.setPnr(getPnrNumber().toString());
			double totalTktAmt = (Integer.parseInt(bookingReqDto.getNoOfTkts())*Integer.parseInt(bookingReqDto.getTicketFare()));
			booking.setTotalFare(totalTktAmt);
			Payment payment = new Payment();
			payment.setUser(user.get());
			payment.setPaymantDate(LocalDate.now());
			payment.setTotalAmount(totalTktAmt);
			payment.setModeOfPayment("GPay");
			paymentRepository.save(payment);
			booking.setStatus(AppConstants.BOOKED);
			BookingDetails bookinD = bookingRepository.save(booking);
			reponse.setStatusCode(AppConstants.BOOKING_CODE);
			reponse.setStatusMsg(AppConstants.BOOKING_MSG);
			reponse.setPnr(bookinD.getPnr());
		} catch (Exception e) {
			reponse.setStatusCode(AppConstants.BOOKING_FAILURE_CODE);
			reponse.setStatusMsg(AppConstants.BOOKING_FAILURE_MSG);
		}
		LOGGER.info("*************Inside saveBooking service layer***************END");
		return reponse;
	}


	private Long getPnrNumber() {
		return (long)(Math.random()*100000 + 4343500000L);
	}

	public List<UserTktBookingDetailsResponseDto> userBookingDetails(Integer userId) {
		LOGGER.info("*************Inside userBookingDetails  service layer***************START");
		List<UserTktBookingDetailsResponseDto> uBookingDetails = new ArrayList<UserTktBookingDetailsResponseDto>();

		User user = new User();
		user.setUserId(userId);
		List<BookingDetails> bDetails =  bookingRepository.findByUser(user);
		if(null!=bDetails && bDetails.size()>0) {
			for (BookingDetails bookingDetails : bDetails) {
				UserTktBookingDetailsResponseDto userbookingHistory = new UserTktBookingDetailsResponseDto();
				Train trinnu = bookingDetails.getTrainId();
				Optional<Train> trainno = trainrRepo.findById(trinnu.getTrainId());
				if(trainno.isPresent()) {
					userbookingHistory.setTrainNumber(trainno.get().getTrainNumber().toString());
					userbookingHistory.setTrainName(trainno.get().getTrainName());
				}
				userbookingHistory.setNoOfTkts(bookingDetails.getNoOfTicket().toString());
				userbookingHistory.setTotalFare(String.valueOf(bookingDetails.getTotalFare()));
				userbookingHistory.setPnr(bookingDetails.getPnr().toString());
				userbookingHistory.setStatus(bookingDetails.getStatus());
				uBookingDetails.add(userbookingHistory);
			}
		}
		LOGGER.info("*************Inside userBookingDetails  service layer***************END");
		return uBookingDetails;
	}

}
