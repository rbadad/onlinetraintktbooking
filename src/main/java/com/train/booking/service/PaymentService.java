package com.train.booking.service;

import com.train.booking.dto.PaymentRequestDto;
import com.train.booking.dto.PaymentResponseDto;

/**
 * @author prabirkumar.jena
 *
 * PaymentService Interface
 */
public interface PaymentService {
	
	public PaymentResponseDto updatePaymentService(PaymentRequestDto paymentRequestDto);
}