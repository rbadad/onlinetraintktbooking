package com.train.booking.service;

import com.train.booking.dto.UserLoginReqDto;
import com.train.booking.dto.UserLoginResponseDto;

/**
 * @author prabirkumar.jena
 *
 */
public interface UserService {
	UserLoginResponseDto userLogin(UserLoginReqDto reqDto);
}
