package com.train.booking.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.train.booking.dto.UserLoginReqDto;
import com.train.booking.dto.UserLoginResponseDto;
import com.train.booking.service.UserServiceImpl;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserServiceImpl serviceImpl;

	@PostMapping("/login")
	public ResponseEntity<UserLoginResponseDto> userLogin(@Valid @RequestBody UserLoginReqDto reqDto) {
		LOGGER.info("UserController userLogin() method called");
		return new ResponseEntity<>(serviceImpl.userLogin(reqDto), HttpStatus.OK);
	}

}
