package com.train.booking.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.train.booking.constants.AppConstants;
import com.train.booking.dto.BookingReqDto;
import com.train.booking.dto.BookingResDto;
import com.train.booking.dto.UserTktBookingDetailsResponseDto;
import com.train.booking.exception.TrainNotFoundException;
import com.train.booking.exception.UserNotFoundException;
import com.train.booking.service.BookingServiceImpl;

import io.swagger.annotations.ApiOperation;


/**
 * @author Raja
 *
 */
@RestController
@RequestMapping("/booking-details")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BookingController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BookingController.class);

	@Autowired
	private BookingServiceImpl bookingServiceImpl;

	@PostMapping("")
	@ApiOperation("Booking trains tickets")
	public BookingResDto saveTktBooking(@Valid @RequestBody BookingReqDto bookingReqDto) {
		LOGGER.info("::::::::Inside Ticket booking service:::::::::::::START");
		BookingResDto bookingResDto = new BookingResDto();
		try {
			if(Integer.parseInt(bookingReqDto.getNoOfTkts())<= AppConstants.FIVE) {
				bookingResDto = bookingServiceImpl.saveBooking(bookingReqDto);
			}else {
				bookingResDto.setStatusCode(AppConstants.MAX_BOOKING_VALIDATION_CODE);
				bookingResDto.setStatusMsg(AppConstants.MAX_BOOKING_VALIDATION_MSG);
			}
		} catch (UserNotFoundException e) {
			e.printStackTrace();
		} catch (TrainNotFoundException e) {
			e.printStackTrace();
		}
		LOGGER.info(":::::::::::SERVICE CALL END:::::::::::::::::");
		return bookingResDto;

	}

	@GetMapping("/{userId}/bookingHistory")
	@ApiOperation("User ticket booking history")
	public List<UserTktBookingDetailsResponseDto> getUserBookingDetails(@RequestParam("userId") Integer userId) {
		LOGGER.info("INside getUserBooking history service:::::: START");
		return bookingServiceImpl.userBookingDetails(userId);

	}

}
