package com.train.booking.controller;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.train.booking.dto.SearchResponseDto;
import com.train.booking.dto.TrainResponseDto;
import com.train.booking.exception.CanNotSearchForPastDaysException;
import com.train.booking.exception.TrainNotFoundException;
import com.train.booking.exception.TrainsNotAvailableException;
import com.train.booking.exception.TrainsNotAvailableGivenDateException;
import com.train.booking.service.TrainService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author janbee
 *
 */
@RestController
@RequestMapping("/trains")
@Api(description = "Operations pertaning to train service")
public class TrainController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TrainController.class);
	@Autowired
	TrainService trainService;

	/**
	 * 
	 * @param source
	 * @param destination
	 * @param journeyDate
	 * @return List<SearchResponseDto>
	 * @throws TrainsNotAvailableException 
	 * @throws TrainsNotAvailableGivenDateException 
	 * @throws CanNotSearchForPastDaysException 
	 */
	@GetMapping
	@ApiOperation("lists available trains based on search key")
	@ApiResponses({@ApiResponse(code = 4001, message = "Trains are not available on this route")
	,@ApiResponse(code = 4002, message = "Trains are not available for given date")
	,@ApiResponse(code = 4003, message = "Can not search for past days, please select proper date!")})
	public ResponseEntity<List<SearchResponseDto>> search(@RequestParam String source, @RequestParam String destination, 
			@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)  LocalDate journeyDate) throws TrainsNotAvailableException, TrainsNotAvailableGivenDateException, CanNotSearchForPastDaysException {
		LOGGER.debug("TrainController :: search :: start");
		List<SearchResponseDto> response = trainService.search(source, destination, journeyDate);
		LOGGER.debug("TrainController :: search :: end");
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param trainId
	 * @return TrainResponseDto
	 * @throws TrainNotFoundException
	 */
	@GetMapping("/{trainId}")
	@ApiOperation("Details of a train")
	@ApiResponses({@ApiResponse(code = 4000, message = "Train is not available")})
	public ResponseEntity<TrainResponseDto> getById(@PathVariable Integer trainId) throws TrainNotFoundException {
		LOGGER.debug("TrainController :: getById :: start");
		TrainResponseDto resposneDto = trainService.getById(trainId);
		LOGGER.debug("TrainController :: getById :: end");
		return new ResponseEntity<>(resposneDto,HttpStatus.OK);
	}
}
