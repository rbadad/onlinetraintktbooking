package com.train.booking.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.train.booking.dto.PaymentRequestDto;
import com.train.booking.dto.PaymentResponseDto;
import com.train.booking.service.GPayService;
import com.train.booking.service.PaymentService;
import com.train.booking.service.PhonePayService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author prabirkumar.jena
 *
 * PaymentController Implementation Class
 */
@Api("Operations for Payments ")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/Payments")
public class PaymentController {
	@Autowired
	ApplicationContext context;
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);
	@ApiOperation("Gateway for Payment")
	@PostMapping
	public ResponseEntity<PaymentResponseDto> updatePaymentService(@Valid @RequestBody PaymentRequestDto payment) {
		LOGGER.info("PaymentController updatePaymentService() method called");
		PaymentService paymentService = null;
		String pType = payment.getModeOfPayment();
		switch (pType) {

		case "GPay":
			paymentService = context.getBean(GPayService.class);
			break;
		case "PhonePay":
			paymentService = context.getBean(PhonePayService.class);
			break;
		default:
			throw new IllegalArgumentException("Payment type not supported");
		}

		PaymentResponseDto response = paymentService.updatePaymentService(payment);
		LOGGER.info("PaymentController updatePaymentService() method call end");
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
}
