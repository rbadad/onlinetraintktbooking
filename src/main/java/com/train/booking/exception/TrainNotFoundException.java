package com.train.booking.exception;

/**
 * @author janbee
 *
 */
public class TrainNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrainNotFoundException() {
		super();
	}
}
