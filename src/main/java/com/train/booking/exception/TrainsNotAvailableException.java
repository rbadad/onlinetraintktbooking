package com.train.booking.exception;

/**
 * @author janbee
 *
 */
public class TrainsNotAvailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrainsNotAvailableException() {
		super();
	}

	public TrainsNotAvailableException(String arg0) {
		super(arg0);
	}

	
}
