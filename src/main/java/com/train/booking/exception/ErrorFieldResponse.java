package com.train.booking.exception;

import java.util.List;

public class ErrorFieldResponse {

	private String message;
	private List<String> details;

	public ErrorFieldResponse(String message, List<String> details) {
		super();
		this.details = details;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}
}