package com.train.booking.exception;

/**
 * @author janbee
 *
 */
public class TrainsNotAvailableGivenDateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrainsNotAvailableGivenDateException() {
		super();
	}

	public TrainsNotAvailableGivenDateException(String arg0) {
		super(arg0);
	}

	
}
