package com.train.booking.exception;

/**
 * @author janbee
 *
 */
public class CanNotSearchForPastDaysException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CanNotSearchForPastDaysException() {
		super();
	}

	public CanNotSearchForPastDaysException(String message) {
		super(message);
	}
	
	

}
