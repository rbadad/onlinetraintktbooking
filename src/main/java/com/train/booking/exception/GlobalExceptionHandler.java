package com.train.booking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.train.booking.constants.AppConstants;

/**
 * @author janbee
 *
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * 
	 * @return error response.
	 */
	@ExceptionHandler(value = TrainNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleTrainNotFoundException() {
		ErrorResponse response = new ErrorResponse();
		response.setMessage(AppConstants.TRAIN_NOT_FOUND1);
		response.setStatus(AppConstants.TRAIN_NOT_FOUND_STATUS_CODE);
		return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = TrainsNotAvailableException.class)
	public ResponseEntity<ErrorResponse> handleTrainsNotAvailableException() {
		ErrorResponse response = new ErrorResponse();
		response.setMessage(AppConstants.TRAINS_NOT_AVAILABLE);
		response.setStatus(AppConstants.TRAINS_NOT_AVAILABLE_STATUS_CODE);
		return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = TrainsNotAvailableGivenDateException.class)
	public ResponseEntity<ErrorResponse> handleTrainsNotAvailableGivenDateException() {
		ErrorResponse response = new ErrorResponse();
		response.setMessage(AppConstants.TRAINS_NOT_AVAILABLE_GIVEN_DATE);
		response.setStatus(AppConstants.TRAINS_NOT_AVAILABLE_STATUS_CODE);
		return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
	}
	
	
	@ExceptionHandler(value = CanNotSearchForPastDaysException.class)
	public ResponseEntity<ErrorResponse> handleCanNotSearchForPastDaysException() {
		ErrorResponse response = new ErrorResponse();
		response.setMessage(AppConstants.CAN_NOT_SEARCH_FOR_PAST_DATE);
		response.setStatus(AppConstants.CAN_NOT_SEARCH_FOR_PAST_DATE_STATUS_CODE);
		return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
	}
}
