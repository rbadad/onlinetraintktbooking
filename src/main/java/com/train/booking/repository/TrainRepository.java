package com.train.booking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.train.booking.entity.Train;

/**
 * 
 * @author janbee
 *
 */
@Repository
public interface TrainRepository extends JpaRepository<Train, Integer>{


	public List<Train> findBySourceAndDestination(String source, String destination);
	
	public Train findByTrainNumber(Integer i);

}
