package com.train.booking.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.train.booking.entity.RunningTrain;
import com.train.booking.entity.Train;

/**
 * @author janbee
 *
 */
@Repository
public interface RunningTrainRepository extends JpaRepository<RunningTrain, Integer>{

	public List<RunningTrain> findByTrain(Train train);
	
	public List<RunningTrain> findByJourneyDate(LocalDate journeyDate);
}
