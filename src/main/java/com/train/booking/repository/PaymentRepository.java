package com.train.booking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.train.booking.entity.Payment;

/**
 * @author prabirkumar.jena
 *
 *PaymentRepository Implementation class
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer> {

}
