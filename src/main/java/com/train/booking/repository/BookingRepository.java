package com.train.booking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.train.booking.entity.BookingDetails;
import com.train.booking.entity.User;


/**
 * @author Raja
 *
 */
@Repository
public interface BookingRepository extends JpaRepository<BookingDetails, Integer> {

	List<BookingDetails> findByUser(User user);

}
